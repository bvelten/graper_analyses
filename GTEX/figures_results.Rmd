---
title: "GTEx example: predicting age using gene expression data from 5 tissues"
author: "Britta Velten"
date: "11/05/2019"
output:
  BiocStyle::html_document:
    toc: true
---

```{r, message=FALSE, warning=FALSE}
library(data.table)
library(DESeq2)
library(ggplot2)
library(dplyr)
library(magrittr)
library(plyr)
library(cowplot)
library(reshape2)
options(stringsAsFactors = FALSE)
library(graper)
library(tidyverse)
library("wesanderson")
library(ggbeeswarm)
source("../util_defs.R") # contains color schemes and methods to include
```

# Perparations
Set input/output paths.
```{r}
datadir <- "data"
outdir <- "2018-11-05"
knitr::opts_chunk$set(fig.path = "figs/", dev = c('png',"pdf"))
```

Load data used for fitting.
```{r}
load(file.path(datadir, "dataGTEx_t5_PCs.RData"))
dim(dataGTEx$X)
```

Load summarised results.
```{r}
load(file.path(outdir,"result_GTEx.RData"))
```

Colors for tissues
```{r}
cols4groups <- c(wes_palette("GrandBudapest1"),
                 wes_palette("GrandBudapest2")[1])
names(cols4groups) <- unique(dataGTEx$annot)
names(cols4groups) <- sub(" ", "\n ", names(cols4groups))
```

# Make plots
## RMSE
Compare prediciton performance between the methods in terms of root mean squared error.
```{r RMSE, fig.widht=8, fig.height=6}
df_RMSE <- melt(sapply(resultList, function(l) l$RMSE),
                varnames = c("method", "run"), value.name="RMSE")
df_RMSE %<>% mutate(method = make_nicenames(method))
df_RMSE %<>% mutate(method_type = ifelse(method%in% methods2compare_sparse,
                                         "sparse", "dense"))  
df_RMSE %<>% filter(method %in% methods2compare_sparse |
                      method %in% methods2compare_dense) 

# make plot 
ggRMSE <- ggplot(df_RMSE, aes(x=method, y=RMSE, fill=method)) +
  geom_boxplot(alpha=0.5, outlier.shape = NA) + 
  # ggtitle ("Prediction of donors' age")  +
  geom_beeswarm(cex = 2.5) + facet_wrap(~method_type, scale="free_x") +
  scale_fill_manual(values = cols4methods) +
  theme(axis.text.x = element_text(angle=60, vjust=1, hjust=1),
        axis.title.x = element_blank()) + guides(fill=FALSE) 
ggRMSE
```

## Penalty Factors
```{r hyperparameters_all, fig.width=12, fig.height=12, eval = FALSE, echo = FALSE}
methods4comp <- c("graper","graper_SS","graper_FF", "IPFLasso", "GRridge")
df_pf <- melt(lapply(resultList, function(l) {
  nms <- rownames(l$pf_mat)
  m <- matrix(as.numeric(l$pf_mat[, methods4comp, drop=F]), ncol=length(methods4comp))
  colnames(m) <- methods4comp
  rownames(m) <- nms
  m
  }),varnames = c("tissue", "method"))

# nice method and tissue names
df_pf %<>% mutate(method=make_nicenames(method))
df_pf %<>% mutate(xtissue=ifelse(tissue == "Adipose Tissue", "Adipose\n Tissue",
                                ifelse(tissue == "Blood Vessel", "Blood\n Vessel", as.character(tissue))))

gg1 <- ggplot(df_pf,aes(x=xtissue, y=value)) + 
    geom_boxplot(alpha =0.5, outlier.shape = NA) +
    ylab(expression(hat(gamma))) + xlab("tissue") +
  geom_beeswarm(aes(col=tissue), groupOnX =TRUE, cex=1.5) +
  facet_wrap(~method, ncol=2) + theme(axis.text.x = element_blank()) +
  theme_bw(base_size = 18) + scale_color_manual(values = cols4groups) #+
  # theme(axis.text.x = element_text(angle=60, vjust=1, hjust=1))

gg1
```


```{r hyperparameters, fig.width=12, fig.height=4}
## Penalty Factors and sparsity level (only in sparse graper)
df_pf <- melt(lapply(resultList, function(l) l$pf_mat[, "graper_SS", drop=F]),
              varnames = c("tissue", "method"))
df_pf$value <- as.numeric(df_pf$value)
df_pf %<>% mutate(tissue=ifelse(tissue == "Adipose Tissue", "Adipose\n Tissue",
                                ifelse(tissue == "Blood Vessel", "Blood\n Vessel", as.character(tissue))))
gg1 <- ggplot(df_pf,aes(x=tissue, y=value)) + 
    geom_boxplot(alpha=0.5, outlier.shape = NA) +
  ylab(expression(hat(gamma))) + geom_beeswarm(aes(col=tissue), cex=2.5)+
  guides(col=FALSE) + xlab("tissue type") + 
  theme_bw(base_size = 15) +
  scale_color_manual(values = cols4groups)

df_pf <- melt(lapply(resultList, function(l) l$sparsity_mat[, "graper_SS", drop=F]),
              varnames = c("tissue", "method"))
df_pf %<>% mutate(tissue=ifelse(tissue == "Adipose Tissue", "Adipose\n Tissue",
                                ifelse(tissue == "Blood Vessel", "Blood\n Vessel", as.character(tissue))))
gg2 <- ggplot(df_pf,aes(x=tissue, y=value)) +
    geom_boxplot(alpha=0.5, outlier.shape = NA) +
  ylab(expression(hat(pi))) + geom_beeswarm(aes(col=tissue), cex=2.5)+
  xlab("tissue type") + guides(col=FALSE) +
  theme_bw(base_size = 15) + scale_color_manual(values = cols4groups)

ggPF <- plot_grid(gg1,gg2, rel_widths = c(1,1.25), nrow=1, align = "hv", axis = "lb")
ggPF
```

## Joint plot
```{r joint, fig.widht=8, fig.height=9}
plot_grid(ggRMSE, ggPF, ncol=1, labels = letters[1:2],
          label_size = 18, rel_heights = c(2,1))
```


# Runtime
```{r time}
time_mat <- sapply(resultList, function(l) l$runtime)
df_time <- melt(time_mat,
                varname=c("method", "fold"), value.name="time")

# only compare to a single iteration for each method (fitted using 3)
df_time %<>% mutate(time=ifelse(grepl("graper", method), time/3/60, time/60))

# nice method names
df_time %<>% mutate(method = make_nicenames(method))
df_time %<>% filter(method %in% c(methods2compare_dense, methods2compare_sparse))

ggplot(df_time, aes(x=method, y=time, col=method)) +
  stat_summary() + ylab("time [min]") + 
  theme_bw(base_size = 15) +
  theme(axis.title.x = element_blank(),
        axis.text.x = element_text(angle=60, vjust=1, hjust=1))+
  scale_color_manual(values = cols4methods)

df_time %>% group_by(method) %>% dplyr::summarize(mean(time))
```

# SessionInfo
```{r}
sessionInfo()
```

